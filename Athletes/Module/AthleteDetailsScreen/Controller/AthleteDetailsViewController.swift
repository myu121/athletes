//
//  AthleteDetailsViewController.swift
//  Q30
//
//  Created by Michael Yu on 3/26/21.
//

import Foundation
import UIKit

class AthleteDetailsViewController: UIViewController {
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!

    var athlete: Athlete?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = athlete?.name
        descriptionLabel.text = athlete?.description
    }
}
