//
//  AthleteListTableViewCell.swift
//  Q30
//
//  Created by Michael Yu on 3/26/21.
//

import UIKit

class AthleteListTableViewCell: UITableViewCell {
    @IBOutlet private weak var nameButton: UIButton!
    
    func configure(name: String) {
        nameButton.setTitle(name, for: .normal)
    }
}
