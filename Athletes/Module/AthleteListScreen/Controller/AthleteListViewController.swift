//
//  AthleteListViewController.swift
//  Q30
//
//  Created by Michael Yu on 3/26/21.
//

import Foundation
import UIKit

class AthleteListViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView! {
        didSet{
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.tableFooterView = UIView()
        }
    }
    private var athletes: [Athlete] = []
    private var sectionDict: [String: [String]] = [:]
    var sections = [Section]()
    
    private func populateDataFromJson() {
        if let path = Bundle.main.path(forResource: "athlete", ofType: "json") {
            do {
                let dataJson = try Data(contentsOf: URL(fileURLWithPath: path))
                let jsonDict = try JSONSerialization.jsonObject(with: dataJson, options: .mutableContainers)
                if let jsonResults = jsonDict as? [[String: String]] {
                    jsonResults.forEach { athleteDict in
                        if let name = athleteDict["name"],
                           let country = athleteDict["country"] {
                            athletes.append(Athlete(name:  name, country: country, description: athleteDict["description"] ?? ""))
                        }
                    }
                }
            } catch {
                print("No json data found")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateDataFromJson()
        tableView.allowsSelection = false
        prepareSectionDict()
        // get the country keys and sort them alphabetically
        let keys = sectionDict.keys.sorted()
        // map the sorted keys to a struct
        sections = keys.map { Section(country: $0, names: sectionDict[$0]!.sorted()) }
    }
    
    func prepareSectionDict() {
        for athlete in athletes {
            if sectionDict.keys.contains(athlete.country) {
                sectionDict[athlete.country]?.append(athlete.name)
            } else {
                sectionDict[athlete.country] = [athlete.name]
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? AthletePopUpViewController {
            let name = ((sender ?? nil) as? UIButton)?.currentTitle
            if let athlete = athletes.first(where: { $0.name == name }) {
                vc.configure(athlete: athlete)
                vc.delegate = self
            }
        }
    }
    
    func openAthleteDetailsView(selectedAthlete: Athlete) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "AthleteDetailsViewController") as? AthleteDetailsViewController {
            vc.athlete = selectedAthlete
                navigationController?.pushViewController(vc, animated: true)
            }
    }
}

extension AthleteListViewController: UITableViewDelegate {
}

extension AthleteListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AthleteListTableViewCell", for: indexPath) as? AthleteListTableViewCell else {
            fatalError("Cell not found")
        }
        let section = sections[indexPath.section]
        let name = section.names[indexPath.row]
        cell.configure(name: name)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].names.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    

//    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
//        return sections.map{$0.country}
//    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].country
    }
}

extension AthleteListViewController: AthletePopUpDelegate {
    func moreButtonTapped(selectedAthlete: Athlete){
        openAthleteDetailsView(selectedAthlete: selectedAthlete)
    }
}
