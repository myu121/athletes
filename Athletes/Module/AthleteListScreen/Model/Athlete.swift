//
//  Athlete.swift
//  Q30
//
//  Created by Michael Yu on 3/26/21.
//

import Foundation

class Athlete {
    var name: String
    var country: String
    var description: String
    
    init(name: String, country: String, description: String) {
        self.name = name
        self.country = country
        self.description = description
    }
}

struct Section {
    let country : String
    let names : [String]
}
