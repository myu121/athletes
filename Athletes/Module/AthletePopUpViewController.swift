//
//  AthletePopUpViewController.swift
//  Q30
//
//  Created by Michael Yu on 3/29/21.
//

import Foundation
import UIKit

class AthletePopUpViewController: UIViewController {
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var countryLabel: UILabel!
    @IBOutlet private weak var popUpView: UIView!
    private var selectedAthlete: Athlete?
    weak var delegate: AthletePopUpDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = selectedAthlete?.name
        countryLabel.text = selectedAthlete?.country
        popUpView.layer.cornerRadius = 15
    }
    
    func configure(athlete: Athlete) {
        self.selectedAthlete = athlete
    }
    
    @IBAction func dismiss(_ sender: Any?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openAthleteDetailsView(_ sender: Any?) {
        self.dismiss(animated: false, completion: {
            if let athlete = self.selectedAthlete{
                self.delegate?.moreButtonTapped(selectedAthlete: athlete)
            }
        })
    }
}

protocol AthletePopUpDelegate: AnyObject {
    func moreButtonTapped(selectedAthlete: Athlete)
}
